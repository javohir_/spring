package uz.javohir.spring.message;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Data
public class Massage {
    private String message;

    public Massage(String message) {
        this.message = message;
    }
    public static ResponseEntity<Massage> succes(){
        return ResponseEntity.ok(new Massage("Succes!"));
    }
    public static ResponseEntity<Massage> fail(){
        return ResponseEntity.badRequest().body(new Massage("Maxsulot nomini kiriting!"));
    }

    public static ResponseEntity<Massage> notFound(String massage){
        return new ResponseEntity<>(new Massage(massage), HttpStatus.NOT_FOUND);
    }
}
