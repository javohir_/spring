package uz.javohir.spring.receiver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;
import uz.javohir.spring.transaction.Transaction;

import javax.annotation.PostConstruct;
import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Objects;

@Component
public class JmsTransactionReceiver implements TransactionReceiver {
    private Logger logger = LogManager.getLogger(JmsTransactionReceiver.class);
//    private JmsTemplate jmsTemplate;
//    private MessageConverter messageConverter;

//    public JmsTransactionReceiver(JmsTemplate jmsTemplate, MessageConverter messageConverter) {
//        this.jmsTemplate = jmsTemplate;
//        this.messageConverter = messageConverter;
//    }

//    @PostConstruct
//    public  void  init() throws JMSException {
//        receive();
//    }

    @JmsListener(destination = "transaction", containerFactory = "topicListener")
    @Override
    public void receive(Transaction transaction) throws JMSException {
//        Message message = jmsTemplate.receive("transaction");
//
//        if (Objects.isNull(message)) {
////            logger.error("Message is null");
//            return;
//        }
//        Transaction transaction = (Transaction) messageConverter.fromMessage(message);
        logger.info("Message is received {} ", transaction.toString());
    }
}
