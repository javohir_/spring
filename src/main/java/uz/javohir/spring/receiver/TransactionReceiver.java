package uz.javohir.spring.receiver;

import uz.javohir.spring.transaction.Transaction;

import javax.jms.JMSException;

public interface TransactionReceiver{
    void receive(Transaction transaction) throws JMSException;
}
