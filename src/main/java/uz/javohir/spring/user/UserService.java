package uz.javohir.spring.user;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import uz.javohir.spring.message.Massage;

import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    public final UserRepo userRepo;
    public final BCryptPasswordEncoder encoder;


    public UserService(UserRepo userRepo, BCryptPasswordEncoder encoder) {
        this.userRepo = userRepo;
        this.encoder = encoder;
    }

    public UserRepo getUserRepo() {
        return userRepo;
    }

    public BCryptPasswordEncoder getEncoder() {
        return encoder;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepo.findByUserName(userName);
        if (userOptional.isPresent()){
            return userOptional.get();
        }
        throw new UsernameNotFoundException("User '" + userName + "' not found.");
    }
    ResponseEntity<Massage> registerUser(UserDto userDto){
        User user = new User(userDto.getUserName(), encoder.encode(userDto.getPassword()), Role.USER);
        userRepo.save(user);
        return Massage.succes();
    }


    public ResponseEntity<Massage> registerAdmin(UserDto userDto) {
        User admin = new User(userDto.getUserName(), encoder.encode(userDto.getPassword()), Role.ADMIN);
        userRepo.save(admin);
        return Massage.succes();
    }
}
