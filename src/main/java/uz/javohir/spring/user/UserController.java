package uz.javohir.spring.user;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.javohir.spring.message.Massage;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/user")
@AllArgsConstructor
public class UserController {
    private UserService userService;

//    public UserController(UserService userService) {
//        this.userService = userService;
//    }

//    public UserController() {
//    }

    @PostMapping("/register")
    public ResponseEntity<Massage> register( @RequestBody UserDto userDto){
        return userService.registerUser(userDto);
    }
    @PostMapping("/registerAdmin")
    public ResponseEntity<Massage> registerAdmins(@RequestBody UserDto userDto){
        return userService.registerAdmin(userDto);
    }

}
