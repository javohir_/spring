package uz.javohir.spring.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.api.scripting.JSObject;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {
    @JsonProperty(value = "username")
    @NotNull
    @Size(min = 1, message = "username cannot be less than 1 symbol")
    private String userName;

    @JsonProperty(value = "password")
    @NotNull
    @Size(min = 3, message = "Password must be at least 3 symbol")
    private String password;

    public UserDto(@NotNull @Size(min = 1, message = "username cannot be less than 1 symbol") String userName,
                   @NotNull @Size(min = 3, message = "Password must be at least 3 symbol") String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
