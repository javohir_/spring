package uz.javohir.spring.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import uz.javohir.spring.productType.ProductType;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDto {
    @JsonProperty(value = "id")
    private Long id;
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "typename")
    private String type;
    @JsonProperty(value = "typeid")
    private Long typeId;

    public ProductDto() {
    }

    public ProductDto(Long id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public ProductDto(Long id, String name, String type, Long typeId) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.typeId = typeId;
    }

    public static Product fromDto(ProductDto productDto, ProductType productType) {
        return new Product(productDto.getId(), productDto.getName(), productType);
    }
    public static ProductDto toDto(Product product){
        return new ProductDto(product.getId(), product.getName(), product.productType.getName());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }
}
