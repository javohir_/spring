package uz.javohir.spring.product;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.javohir.spring.message.Massage;

import java.util.List;

@RestController
@RequestMapping(value = "api/product", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {
 private  final ProductService productService;

 public ProductController(ProductService productService) {
  this.productService = productService;
 }

    @GetMapping("/getAllProduct")
    public ResponseEntity<List<ProductDto>> getAllProduct(){
    return productService.getProducts();
    }

    @GetMapping("/byType/{typeId}")
    public ResponseEntity<List<ProductDto>> getProductsByType(@PathVariable Long typeId){
     return productService.getProductsByType(typeId);
    }

    @PostMapping("/addNewProduct")
    public ResponseEntity<Massage> addNewProducts(@RequestBody ProductDto productDto){
      return productService.addProduct(productDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Massage> deleteProduct(@PathVariable("id") Long id){
     return productService.deleteProduct(id);
    }
}
