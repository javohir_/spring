package uz.javohir.spring.product;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import uz.javohir.spring.message.Massage;
import uz.javohir.spring.productType.ProductType;
import uz.javohir.spring.productType.ProductTypeService;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class ProductService {
    private final ProductRepo productRepo;
    private final ProductTypeService productTypeService;

    public ProductService(ProductRepo productRepo, ProductTypeService productTypeService) {
        this.productRepo = productRepo;
        this.productTypeService = productTypeService;
    }

//        @PostConstruct
//    public void init(){
//        if (productRepo.findAll().isEmpty()){
//            productRepo.saveAll(List.of(
//                    new Product("hp"),
//                    new Product("mouse asas"),
//                    new Product("Iphone x ")
//            ));
//        }
//    }

    public ResponseEntity<List<ProductDto>> getProducts(){
        return ResponseEntity.ok(productRepo.findAll()
                .stream()
                .map(ProductDto :: toDto)
                .collect(Collectors.toList()));
    }

    public ResponseEntity<Massage> addProduct(ProductDto productDto){
        if (StringUtils.isEmpty(productDto.getName())){
            return Massage.fail();
        }
        Optional<ProductType> productTypeOptional = productTypeService.getById(productDto.getTypeId());
        if (productTypeOptional.isEmpty()){
            return Massage.notFound("product type not found");
        }
        ProductType productType = productTypeOptional.get();
        System.out.println(productRepo.save(ProductDto.fromDto(productDto, productType)));
        return Massage.succes();
    }

    public ResponseEntity<Massage> deleteProduct(Long id) {
        productRepo.deleteById(id);
        return Massage.succes();
    }

    public ResponseEntity<List<ProductDto>> getProductsByType(Long typeId) {
        return ResponseEntity.ok(productRepo.findByProductTypeId(typeId)
                .stream()
                .map(ProductDto :: toDto)
                .collect(Collectors.toList()));
    }
}
