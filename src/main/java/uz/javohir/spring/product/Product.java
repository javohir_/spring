package uz.javohir.spring.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import uz.javohir.spring.productType.ProductType;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = ("products"))
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
     private Long id;

    @NonNull
    @Column(name = ("name"))
     private String name;

    @OneToOne(targetEntity = ProductType.class)
    @JoinColumn(name = "product_type_id")
    public ProductType productType;

    public Product(@NonNull String name) {
        this.name = name;
    }
    public Product(){

    }

    public Product(Long id, String name, ProductType productType) {
        this.id = id;
        this.name = name;
        this.productType = productType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }


}
