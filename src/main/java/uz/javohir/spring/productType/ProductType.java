package uz.javohir.spring.productType;

import lombok.Data;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import uz.javohir.spring.product.Product;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = ("product_type"))
public class ProductType {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(targetEntity = Product.class)
    private List<Product> productList;

    public ProductType(String name) {
        this.name = name;
    }

    public ProductType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
