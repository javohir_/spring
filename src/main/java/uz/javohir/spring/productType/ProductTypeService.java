package uz.javohir.spring.productType;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import uz.javohir.spring.message.Massage;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

@Service
public class ProductTypeService {
    public ProductTypeRepo productTypeRepo;

    public ProductTypeService(ProductTypeRepo productTypeRepo) {
        this.productTypeRepo = productTypeRepo;
    }

//    @PostConstruct
//    public void init(){
//        if(productTypeRepo.findAll().isEmpty()){
//            productTypeRepo.saveAll(List.of(
//                    new ProductType("Notebook"),
//                    new ProductType("Phone"),
//                    new ProductType("Mouse")));
//        }
//    }

    public List<ProductType> getAllProductTypes(){
        return productTypeRepo.findAll();
    }

    public Optional<ProductType> getById(Long typeId) {
        return productTypeRepo.findById(typeId);
    }

    public List<ProductType> getProductsTypes() {
        return productTypeRepo.findAll();
    }

    public ResponseEntity<Massage> addProductType(ProductType productType) {
        if (StringUtils.isEmpty(productType.getName())){
            return Massage.fail();
        }
        System.out.println(productTypeRepo.save(productType));
        return Massage.succes();
    }
}
