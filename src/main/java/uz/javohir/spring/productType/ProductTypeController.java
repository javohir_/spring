package uz.javohir.spring.productType;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.javohir.spring.message.Massage;
import uz.javohir.spring.product.Product;

import java.awt.*;
import java.util.List;

@RestController
@RequestMapping(value = "api/productType", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductTypeController {
    private final ProductTypeService productTypeService;

    public ProductTypeController(ProductTypeService productTypeService) {
        this.productTypeService = productTypeService;
    }

    @GetMapping("/getAllProductTypes")
    public ResponseEntity<List<ProductType>> getAllProductTypes(){
        return  ResponseEntity.ok(productTypeService.getProductsTypes());
    }

    @PostMapping(value = "/addNewProductType", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Massage> addNewProductType(@RequestBody ProductType productType){
        return productTypeService.addProductType(productType);
    }
}
